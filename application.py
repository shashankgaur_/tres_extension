"""
Shashank Gaur
"""

import sys
from coap import get,put,post
from twisted.internet.defer import Deferred
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.python import log

import txthings.coap as coap
import txthings.resource as resource

success = True

resourcelist = dict()
resourcelist["temperature"] = ["<coap://[aaaa::200:0:0:3]/sensor>","<coap://[aaaa::200:0:0:5]/sensor>"]
resourcelist["actuator"]=["<coap://[aaaa::200:0:0:4]/actuator>"]


def checkresource(protocol, target, uripath):
    #log.startLogging(sys.stdout)
    #endpoint = resource.Endpoint(None)
    #protocol = coap.Coap(endpoint)
    client = get.Action(protocol,target,uripath)
    #reactor.listenUDP(0, protocol, interface='::0')
    #reactor.run()


def assignresource(protocol, target, uripath, payload):
    #log.startLogging(sys.stdout)
    #endpoint = resource.Endpoint(None)
    #protocol = coap.Coap(endpoint)
    client = put.Action(protocol, target, uripath, payload)
    #rea   ctor.listenUDP(0, protocol, interface='::0')
    #reactor.run()


def postresource(protocol, target, uripath, payload):
    #log.startLogging(sys.stdout)
    #endpoint = resource.Endpoint(None)
    #protocol = coap.Coap(endpoint)
    client = post.Action(protocol, target, uripath, payload)
    #reactor.listenUDP(0, protocol, interface='::0')
    #reactor.run()



hostdevice = raw_input("Provide IPV6 for the host device: ")
taskuri = raw_input("Provide task uri on host uri: ")
print "Available resources are "
print resourcelist
inputsourcetype = raw_input("Provide What type of Input source is required: ")
outputdevicetype = raw_input("Provide What type of Input source is required: ")
processingfunction = "/home/user/contiki-tres/examples/tres/bin/halve.pyc"
# raw_input("Provide address of the file for Processing Function: ")
log.startLogging(sys.stdout)
endpoint = resource.Endpoint(None)
protocol = coap.Coap(endpoint)
#client = get.Action(protocol,target,uripath)
assignresource(protocol, hostdevice, taskuri, payload="")
checkresource(protocol, hostdevice, taskuri)
assignresource(protocol, hostdevice, taskuri+"/is", payload=resourcelist[inputsourcetype][0])
assignresource(protocol, hostdevice, taskuri+"/od", payload=resourcelist[inputsourcetype][0])
assignresource(protocol, hostdevice, taskuri+"/pf", payload=open(processingfunction).read())
postresource(protocol, hostdevice, taskuri, payload="Please run it")
reactor.listenUDP(0, protocol, interface='::0')
reactor.run()
assignresource(hostdevice, taskuri, payload="")
checkresource(hostdevice, taskuri)
assignresource(hostdevice, taskuri+"/is", payload=resourcelist[inputsourcetype][0])
assignresource(hostdevice, taskuri+"/od", payload=resourcelist[inputsourcetype][0])
assignresource(hostdevice, taskuri+"/pf", payload=open(processingfunction).read())
postresource(hostdevice, taskuri, payload="Please run it")




