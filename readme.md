Note: This is an on-going work. If you have issues please contact at sgaur@isep.ipp.pt. Shashank Gaur

To install the extension please clone this repository

git clone https://https://bitbucket.org/shashankgaur_/tres_extension.git

Make sure you have contiki-tres installed from: https://github.com/tecip-nes/contiki-tres

After that, just execute the application.py python script, and you are ready to go.