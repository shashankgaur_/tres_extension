'''
Shashank Gaur
'''

import sys
from optparse import OptionParser
from twisted.internet.defer import Deferred
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.python import log

import txthings.coap as coap
import txthings.resource as resource
#
# parser = OptionParser()
#
# parser.add_option("-t", "--TargetAddress", dest="TargetAddress",
#                   help="target IPV6 address", default="aaaa::200:0:0:2")
# parser.add_option("-u", "--URIPath", dest="URIPath",
#                   help="URI Path at the address e.g. tasks/halve", default="tasks/halve/is")
# #parser.add_option("-p", "--Payload", dest="payload",
#                   #help="payload to be delivered", default="<coap://[aaaa::200:0:0:3]/sensor>")
#
# (options, args) = parser.parse_args()
#
# Target= options.TargetAddress
# URI = options.URIPath
#payload = options.payload


class Action():
    "Class for particular coap action such as PUT/GET/POST etc"
    def __init__(self, protocol, target, uripath):
        self.protocol = protocol
        self.target = target
        self.uripath = uripath
        reactor.callLater(1, self.getResource)

    def getResource(self):
        #payload = options.payload
        request = coap.Message(code=coap.GET)
        request.opt.uri_path = (self.uripath,)
        request.opt.observe = 0
        request.remote = (self.target, coap.COAP_PORT)
        d = self.protocol.request(request, observeCallback=self.printLaterResponse)
        d.addCallback(self.printResponse)
        d.addErrback(self.noResponse)


    def printResponse(self, response):
        print 'First result: ' + response.payload
        #reactor.stop()

    def printLaterResponse(self, response):
        print 'Observe result: ' + response.payload

    def noResponse(self, failure):
        print 'Failed to fetch resource:'
        print failure
        #reactor.stop()

# log.startLogging(sys.stdout)
#
# endpoint = resource.Endpoint(None)
# protocol = coap.Coap(endpoint)
# client = Action(protocol)
#
# reactor.listenUDP(0, protocol, interface='::0')
# reactor.run()



