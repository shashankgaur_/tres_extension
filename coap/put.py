'''
Shashank Gaur
'''

import sys
from optparse import OptionParser
from twisted.internet.defer import Deferred
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.python import log

import txthings.coap as coap
import txthings.resource as resource
#
# parser = OptionParser()
#
# parser.add_option("-t", "--TargetAddress", dest="TargetAddress",
#                   help="target IPV6 address")
# parser.add_option("-u", "--URIPath", dest="URIPath",
#                   help="URI Path at the address e.g. tasks/halve")
# parser.add_option("-p", "--Payload", dest="payload",
#                   help="payload to be delivered")
# parser.add_option("-f", "--file", dest="filename",
#                   action="store_true",help="provide it if payload is a file", default=False)
#
#
# (options, args) = parser.parse_args()
#
# Target= options.TargetAddress
# URI = options.URIPath
# if options.filename:
#     payload = open(options.payload).read()
# else:
#     payload = options.payload



class Action():
    "Class for particular coap action such as PUT/GET/POST etc"
    def __init__(self, protocol, target, uripath, payload):
        self.protocol = protocol
        self.target = target
        self.uripath = uripath
        self.payload = payload
        reactor.callLater(1, self.putResource)

    def putResource(self):
        #payload = options.payload
        request = coap.Message(code=coap.PUT, payload=self.payload)
        request.opt.uri_path = (self.uripath,)
        request.opt.content_format = coap.media_types_rev['text/plain']
        request.remote = (self.target, coap.COAP_PORT)
        d = self.protocol.request(request)
        d.addCallback(self.printResponse)


    def printResponse(self, response):
        print 'Response Code: ' + coap.responses[response.code]
        print 'Payload: ' + response.payload
        #if coap.responses[response.code] ==
        #reactor.stop()

# log.startLogging(sys.stdout)
#
# endpoint = resource.Endpoint(None)
# protocol = coap.Coap(endpoint)
# client = Action(protocol)
#
# reactor.listenUDP(0, protocol, interface='::0')
# reactor.run()



