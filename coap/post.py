'''
Shashank Gaur
'''

import sys
from optparse import OptionParser
from twisted.internet.defer import Deferred
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.python import log

import txthings.coap as coap
import txthings.resource as resource

# parser = OptionParser()
#
# parser.add_option("-t", "--TargetAddress", dest="TargetAddress",
#                   help="target IPV6 address", default="aaaa::200:0:0:2")
# parser.add_option("-u", "--URIPath", dest="URIPath",
#                   help="URI Path at the address e.g. tasks/halve", default="tasks/halve/is")
# parser.add_option("-p", "--Payload", dest="payload",
#                   help="payload to be delivered", default="")
#
# (options, args) = parser.parse_args()
#
# Target= options.TargetAddress
# URI = options.URIPath
# payload = options.payload


class Action():
    "Class for particular coap action such as PUT/GET/POST etc"
    def __init__(self, protocol, target, uripath, payload):
        self.protocol = protocol
        self.target = target
        self.uripath = uripath
        self.payload = payload
        reactor.callLater(1, self.postResource)

    def postResource(self):
        #payload = options.payload
        request = coap.Message(code=coap.POST, payload=self.payload)
        request.opt.uri_path = (self.uripath,)
        request.opt.content_format = coap.media_types_rev['text/plain']
        request.remote = (self.target, coap.COAP_PORT)
        d = self.protocol.request(request)
        d.addCallback(self.printResponse)


    def printResponse(self, response):
        print 'Response Code: ' + coap.responses[response.code]
        print 'Payload: ' + response.payload
        #reactor.stop()

# log.startLogging(sys.stdout)
#
# endpoint = resource.Endpoint(None)
# protocol = coap.Coap(endpoint)
# client = Action(protocol)
#
# reactor.listenUDP(0, protocol, interface='::0')
# reactor.run()




